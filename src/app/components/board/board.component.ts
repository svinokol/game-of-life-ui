import { Component, Input } from "@angular/core";
import { Game } from "../../models/game";
import { Cell } from "../../models/cell";

@Component({
    selector: 'board',
    templateUrl: 'board.component.html'
})
export class BoardComponent {

    @Input()
    game: Game;

    range(min, max, step) {
        step = step || 1;
        var input = [];
        for (var i = min; i <= max; i += step) {
            input.push(i);
        }
        return input;
    }

    onCellClick(i: number, j: number) {
        if(!this.isActiveCell(i,j)) {
            let cell = new Cell();
            cell.x = i;
            cell.y = j;
            this.game.activeCells.push(cell);
        }else {
            this.removeCell(i,j);
        }
    }

    removeCell(i: number, j: number) {
        for(let k = 0; k < this.game.activeCells.length; k++){
            let cell = this.game.activeCells[k];
            if(cell.x === i && cell.y === j ){
               this.game.activeCells.splice(k,1);
            }
        }
    }

    isActiveCell(x: number, y: number): boolean {
        for(let i = 0; i < this.game.activeCells.length; i++){
            let cell = this.game.activeCells[i];
            if(cell.x === x && cell.y === y ){
                return true;
            }
        }
        return false;
    }
}